﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemyCharacter
{
    public class AutomatonEnemyAI : EnemyAI
    {

        // Use this for initialization
        void Start()
        {
            //setup the sphere collider as a trigger with the detailed perception radius
            perceptionCollider = GetComponent<SphereCollider>();
            perceptionCollider.isTrigger = true;
            perceptionCollider.radius = perceptionRadius;

            //get the navagent and positions for the wandering behaviour
            navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            startPosition = transform.position;
            targetPosition = startPosition;


            rangedAttackDelayTimeMax = Random.value * 5.0f;
            rangedAttackDelayTime = rangedAttackDelayTimeMax;

            //m_Target = GameObject.FindGameObjectWithTag("Player");
            regainTimeMax = (Random.value * 10.5f) + 5.0f;
            regainTime = regainTimeMax;

            startPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {

            switch (aiState)
            {
                case ENEMYSTATES.IDLE:
                    Wander();
                    break;
                case ENEMYSTATES.AVOID:
                    DoAvoid();
                    break;
                case ENEMYSTATES.COMBAT:
                    DoAttack();
                    break;
                case ENEMYSTATES.SEEK:
                    DoSeek();
                    break;
                case ENEMYSTATES.KO:
                    DoFaint();
                    break;
                case ENEMYSTATES.DEATH:
                    DoDie();
                    break;
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            //if (other.gameObject.position )

            if (other.gameObject.tag == "Player")
            {
                m_Target = other.gameObject;
                aiState = ENEMYSTATES.COMBAT;
            }
        }

        //set seeking mode and navigation towards collision point to wander from there
        private void OnTriggerExit(Collider other)
        {
            //if the player moves out of range the enemy will move to the last position and wander from there
            if (other.gameObject.tag == "Player")
            {
                m_Target = null;
                targetPosition = other.transform.position;
                startPosition = other.transform.position;
                navAgent.SetDestination(targetPosition);
                aiState = ENEMYSTATES.SEEK;

            }
        }


        private void DoAttack()
        {
            if (m_Target == null)
            {
                AiState = ENEMYSTATES.IDLE;
                return;
            }

            //range attack
            if ((m_Target.transform.position - transform.position).magnitude >= 25.0f)
            {
                rangedAttackDelayTime -= Time.deltaTime;
                Debug.Log(rangedAttackDelayTime);

                if (rangedAttackDelayTime <= 0)
                {
                    if (m_Target != null)
                    {
                        Vector3 dir = m_Target.transform.position - transform.position;
                        Debug.LogWarning("range");
                        RangeAttack(dir);
                    }

                    rangedAttackDelayTime = rangedAttackDelayTimeMax;
                }
            }
            else
            {
                //close combat
            }
        }

        private void DoSeek()
        {
            //move to last known position
            //and wander from there

            //obsolete since the onTrigerExit function
        }

        private void DoAvoid()
        {
            //keep player in range while staying away for amount of time
        }

        //get ko'ed once health reaches less than specified amount
        private void DoFaint()
        {
            regainTime -= Time.deltaTime;

            if (regainTime <= 0.0f)
            {
                AiState = ENEMYSTATES.AVOID;
            }
        }

        //destroy the enemy if health is depleted
        private new void DoDie()
        {
            //play animation here

            Destroy(gameObject);
        }

    }
}
