﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class ApplicationModel
{
    static internal float slowDownFixedUpdateTime = 0.002f;
    static internal float slowDownUpdateTime = 0.1f;
    static internal float normalFixedUpdateTime = 0.02f;
    static internal float normalUpdateTime = 1f;

    static internal float lightAttackValue = 10f;
    static internal float heavyAttackValue = 20f;
    static internal float enemyKOPercentage = 0.1f;

    //rotate a point around a world point for an angle
    static internal Vector3 RotateAroundPivot(float angle, Vector3 position, Vector3 startPosition)
    {
        return Quaternion.Euler(angle * Vector3.up) * (position - startPosition) + startPosition;
    }

}
