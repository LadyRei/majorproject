﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemyCharacter
{
    [RequireComponent(typeof(SphereCollider))]
    public class Projectile : MonoBehaviour
    {
        private float projectileDamage;
        private Vector3 direction = Vector3.zero;
        private float movementSpeed = 5f;
        private SphereCollider m_Collider;


        // Use this for initialization
        void Start()
        {
            m_Collider = GetComponent<SphereCollider>();
        }

        // Update is called once per frame
        void Update()
        {
            if (direction != Vector3.zero)
            { 
                Vector3 nPos = transform.position + (movementSpeed * direction) * Time.deltaTime;
                transform.position = new Vector3(nPos.x, nPos.y, nPos.z);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            DisplayParticles();

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.GetComponent<PlayerCharacter.PlayerCombat>().TakeDamage(projectileDamage);
            }

            Destroy(gameObject);
        }

        private void DisplayParticles()
        {
            
        }

        public Vector3 Direction
        {
            get
            {
                return direction;
            }

            set
            {
                direction = value;
            }
        }

        public float ProjectileDamage
        {
            get
            {
                return projectileDamage;
            }

            set
            {
                Mathf.Clamp(value, 0f, 100f);
                projectileDamage = value;
            }
        }
    } 
}
