﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerCharacter
{
    //[RequireComponent(typeof(PlayerCharacter.PlayerMovement))]
    //[RequireComponent(typeof(PlayerCharacter.PlayerCombat))]

    public class PlayerInput : MonoBehaviour
    {
        private Transform m_Camera;

        private bool jumpPressed = false;
        private bool dashPressed = false;
        private Vector3 moveVector = Vector3.zero;
        //private Vector3 moveCamVector = Vector3.zero;
        private float horizontalCamera = 0.0f;
        private float verticalCamera = 0.0f;
        private bool lightAttack = false;
        private bool heavyAttack = false;
        private bool killAttack = false;

        // Use this for initialization
        void Start()
        {
            m_Camera = Camera.main.transform;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            float horizontalAxis = Input.GetAxis("Horizontal");
            float verticalAxis = Input.GetAxis("Vertical");
            horizontalCamera = Input.GetAxis("CamHorizontal");
            verticalCamera = Input.GetAxis("CamVertical");

            Vector3 camForward = Vector3.ProjectOnPlane(m_Camera.transform.forward, Vector3.up); //since camera can be at an angle, project movement onto plane that has normal vector.up
            Vector3 camRight = m_Camera.transform.right;

            Vector3 direction = horizontalAxis * camRight + verticalAxis * camForward;
            moveVector = direction.normalized;

            //moveCamVector = (HorizontalCamera * Vector3.right + VerticalCamera * Vector3.up).normalized;
            
            jumpPressed = Input.GetButtonDown("Jump");
            dashPressed = Input.GetButtonDown("Dash");
            lightAttack = Input.GetButtonDown("Light");
            heavyAttack = Input.GetButtonDown("Heavy");
            //killAttack = Input.GetButtonDown("Kill");

            //if (lightAttack)
            //    Debug.LogWarning("light");
            //if (heavyAttack)
            //    Debug.LogWarning("heavy");
            //if (killAttack)
            //    Debug.LogWarning("kill");


            //Debug.Log(horizontalAxis + " " + verticalAxis + " " + horizontalCamera + " " + verticalCamera + " " + jumpPressed + " " + dashPressed + " " + lightAttack + " " + heavyAttack + " " + killAttack);
        }

        public bool JumpPressed
        {
            get
            {
                return jumpPressed;
            }

            set
            {
                if (value == true)
                    return;

                jumpPressed = value;
            }
        }

        public bool DashPressed
        {
            get
            {
                return dashPressed;
            }

            set
            {
                if (value == true)
                    return;

                dashPressed = value;
            }
        }

        public bool LightAttack
        {
            get
            {
                return lightAttack;
            }

            set
            {
                if (value == true)
                    return;

                lightAttack = value;
            }
        }

        public bool HeavyAttack
        {
            get
            {
                return heavyAttack;
            }

            set
            {
                if (value == true)
                    return;

                heavyAttack = value;
            }
        }

        public bool KillAttack
        {
            get
            {
                return killAttack;
            }

            set
            {
                if (value == true)
                    return;

                killAttack = value;
            }
        }

        public Vector3 MoveVector
        {
            get
            {
                return moveVector;
            }
        }

        public float HorizontalCamera
        {
            get
            {
                return horizontalCamera;
            }
        }

        public float VerticalCamera
        {
            get
            {
                return verticalCamera;
            }
        }

        //public Vector3 MoveCamVector
        //{
        //    get
        //    {
        //        return moveCamVector;
        //    }
        //}

    }

}