﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemyCharacter
{
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class EnemyAI : MonoBehaviour
    {
        public GameObject rangedProjectile;
        public float damage;
        protected float rangedAttackDelayTime, rangedAttackDelayTimeMax;
        protected float regainTime, regainTimeMax;
        protected float rangeAttackDistance = 5.0f; //was 25
        protected float positionCloseEnough = 5.0f;

        protected SphereCollider perceptionCollider;
        protected float perceptionRadius = 25.0f;
        protected UnityEngine.AI.NavMeshAgent navAgent;
        protected GameObject m_Target;
        protected Vector3 startPosition;
        protected Vector3 targetPosition;
        protected float wanderDistanceMax = 10.0f; //was 50.0f;
        protected ENEMYSTATES aiState = ENEMYSTATES.IDLE;

        internal ENEMYSTATES AiState
        {
            get
            {
                return aiState;
            }

            set
            {
                aiState = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        internal void ChangeState(ENEMYSTATES newState)
        {
            aiState = newState;
        }

        protected void RangeAttack(Vector3 direction)
        {
            Vector3 pos = transform.position;
            Vector3 forw = transform.forward;
            Quaternion rot = transform.rotation;

            Debug.LogWarning("spawn" + direction);
            GameObject go = Instantiate(rangedProjectile, pos + forw, rot);
            Destroy(go, 5);

            Projectile proj = go.GetComponent<Projectile>();
            proj.Direction = direction;
            proj.ProjectileDamage = damage;

        }

        protected void Wander()
        {

        }

        protected void DoDie()
        {
            //play animation here

            Destroy(gameObject);
        }

    }

    public enum ENEMYSTATES
    {
        IDLE,
        AVOID,
        SEEK,
        COMBAT,
        KO,
        DEATH,
        REGAIN, 
        MERGE
    }

}