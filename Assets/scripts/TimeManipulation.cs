﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManipulation : MonoBehaviour {

    public bool isOn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setSlowdownTime(bool slow)
    {
        if (isOn)
        {
            if (slow)
            {
                Time.timeScale = ApplicationModel.slowDownUpdateTime;
                Time.fixedDeltaTime = ApplicationModel.slowDownFixedUpdateTime;
            }
            else
            {
                Time.timeScale = ApplicationModel.normalUpdateTime;
                Time.fixedDeltaTime = ApplicationModel.normalFixedUpdateTime;
            } 
        }
    }
}
