﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerCharacter
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(TimeManipulation))]
    public class PlayerCombat : MonoBehaviour
    {
        private BoxCollider m_frontAttackCollider;
        private SphereCollider m_backAttackCollider;

        //private Rigidbody m_Rigidbody;
        //private CapsuleCollider m_ModelCollider;
        private Renderer m_Renderer;
        private PlayerInput m_Input;
        private TimeManipulation m_Time;

        //private const float lightAttackValue = 10f;
        //private const float heavyAttackValue = 20f;
        //private bool lightAttackCooldown = false;
        //private bool heavyAttackCooldown = false;

        private float health, healthMax = 100f;

        // Use this for initialization
        void Start()
        {
            m_frontAttackCollider = GetComponent<BoxCollider>();
            m_backAttackCollider = GetComponent<SphereCollider>();

            //m_Rigidbody = GetComponent<Rigidbody>();
            //m_ModelCollider = GetComponent<CapsuleCollider>();
            m_Renderer = GetComponent<Renderer>();
            m_Input = GetComponent<PlayerInput>();
            m_Time = GetComponent<TimeManipulation>();

            health = healthMax;
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void TakeDamage(float damage)
        {
            //need to implement health better
            health -= damage;

            StartCoroutine(DisplayDamage());

            if (health <= 0.0f)
            {
                //death
                DisplayDeath();
            }

        }

        private void DisplayDeath()
        {
            
        }

        private IEnumerator DisplayDamage()
        {
            m_Renderer.material.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            m_Renderer.material.color = Color.white;
            yield return new WaitForSeconds(0.2f);
            m_Renderer.material.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            m_Renderer.material.color = new Color32(96, 44, 192, 255);
            yield return new WaitForSeconds(0.2f);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Enemy")
                AttackEnemy(collision);
        }

        private void OnCollisionStay(Collision collision)
        {
            if (collision.gameObject.tag == "Enemy")
                AttackEnemy(collision);
        }

        private void AttackEnemy(Collision collision)
        {
            if (m_Input.HeavyAttack && collision.contacts[0].thisCollider == m_backAttackCollider)
            {
                StartCoroutine(UpdateAnimation(ATTACKS.KILL));
                Debug.LogWarning("kill active");
                m_Input.KillAttack = false;
                Attack(collision.gameObject, false, true);
            }
            else if (m_Input.LightAttack && collision.contacts[0].thisCollider == m_frontAttackCollider)
            {
                StartCoroutine(UpdateAnimation(ATTACKS.LIGHT));
                Debug.LogWarning("light active");
                m_Input.LightAttack = false;
                Attack(collision.gameObject, true, false);
            }
            else if (m_Input.HeavyAttack && collision.contacts[0].thisCollider == m_frontAttackCollider)
            {
                StartCoroutine(UpdateAnimation(ATTACKS.HEAVY));
                Debug.LogWarning("heavy active");
                m_Input.HeavyAttack = false;
                Attack(collision.gameObject, false, false);
            }
            //else if (collision.contacts[0].thisCollider.GetType() != typeof(CapsuleCollider))
            //{
            //    Debug.Log(collision.gameObject.tag);
            //}
        }

        private IEnumerator UpdateAnimation(ATTACKS attack)
        {
            m_Time.setSlowdownTime(true);

            switch(attack)
            {
                case ATTACKS.HEAVY:
                    yield return new WaitForSeconds(0.6f);
                    break;
                case ATTACKS.LIGHT:
                    yield return new WaitForSeconds(0.2f);
                    break;
                //case ATTACKS.KILL:
                //    yield return new WaitForSeconds(1);
                //    break;
                default:
                    yield return new WaitForEndOfFrame();
                    break;
            }

            m_Time.setSlowdownTime(false);
        }

        private void Attack(GameObject enemy, bool light, bool kill)
        {
            TakeDamage damageScript = enemy.GetComponent<TakeDamage>();
            if(damageScript != null)
            {
                //if (kill)
                //    damageScript.Damage(heavyAttackValue, true);
                //else 
                if (light)
                    damageScript.Damage(ApplicationModel.lightAttackValue, false);
                else
                    damageScript.Damage(ApplicationModel.heavyAttackValue, false);
            }
        }
    } 

    enum ATTACKS
    {
        LIGHT, 
        HEAVY,
        KILL
    }
}
