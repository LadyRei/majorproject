﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerCharacter
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerCamera : MonoBehaviour
    {
        //public float cameraDistance = 5.0f;

        private GameObject m_Player;
        private PlayerInput m_Input;

        private float radiusTarget = 10.0f;
        private float azim = 0.0f, elev = 40.0f;

        // Use this for initialization
        void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_Input = m_Player.GetComponent<PlayerInput>();

            Debug.LogWarning(m_Player);

            float azimAngle = Mathf.Deg2Rad * (azim - 90.0f);
            float elevAngle = Mathf.Deg2Rad * (elev - 20.0f);

            Vector3 deltaCamPos = new Vector3(radiusTarget * Mathf.Cos(azimAngle), radiusTarget * Mathf.Sin(elevAngle), radiusTarget * Mathf.Sin(azimAngle));
            Vector3 camPos = m_Player.transform.position + deltaCamPos;

            transform.position = camPos;
            transform.LookAt(m_Player.transform);
        }

        // Update is called once per frame
        void Update()
        {
            float moveUp = m_Input.VerticalCamera;
            float moveRight = m_Input.HorizontalCamera;

            UpdateCamPosition(moveRight, moveUp);

            float azimAngle = Mathf.Deg2Rad * (azim - 90.0f);
            float elevAngle = Mathf.Deg2Rad * (elev - 20.0f);

            Vector3 deltaCamPos = new Vector3(radiusTarget * Mathf.Cos(azimAngle), radiusTarget * Mathf.Sin(elevAngle), radiusTarget * Mathf.Sin(azimAngle));
            Vector3 camPos = m_Player.transform.position + deltaCamPos;

            transform.position = camPos;

            transform.LookAt(m_Player.transform);
        }

        void UpdateCamPosition(float h, float v)
        {
           //int invertedX = 1;
           //int invertedY = 1;
           //if (invertableX)
           //    invertedX = -1;
           //if (invertableY)
           //    invertedY = -1;
           //azim += 100 * invertedX * h * Time.deltaTime;
           //elev += 100 * invertedY * v * Time.deltaTime;

           azim += 200 * h * Time.deltaTime;
           elev += 200 * v * Time.deltaTime;

           if (azim > 360.0f)
               azim = 0.0f;

           if (elev > 110.0f)
               elev = 110.0f;

           if (elev < 0.0f)
               elev = 0.0f;
        }
    } 
}
