﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerCharacter
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(TimeManipulation))]
    public class PlayerMovement : MonoBehaviour
    {
        private Rigidbody m_Rigidbody;
        //private CapsuleCollider m_Collider;
        private PlayerInput m_Input;
        private TimeManipulation m_Time;

        public float movementSpeed;
        private float dashSpeed;
        private float dashTimer = 0.0f, dashCooldown = 5.0f;
        public float jumpForce;
        private bool isGrounded = true;

        //adjust this to whatever the model halfheight is
        private float m_GroundCheckDistance = 1.1f;

        // Use this for initialization
        void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
            //m_Collider = GetComponent<CapsuleCollider>();
            m_Input = GetComponent<PlayerInput>();
            m_Time = GetComponent<TimeManipulation>();

 /*this is broken since new movement*/           dashSpeed = 10 * movementSpeed;

        }

        // Update is called once per frame
        void Update()
        {
            CheckGroundStatus();
            CheckDashCooldown();

            Vector3 move = m_Input.MoveVector;

            if (move != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(move);

            //if grounded, jump
            if (isGrounded == true && m_Input.JumpPressed == true)
            {
                m_Rigidbody.AddForce(jumpForce*Vector3.up, ForceMode.Impulse);
                isGrounded = false;
                Debug.LogWarning("Jump");
            }
            m_Input.JumpPressed = false;

            //if not dashing, dash
            if (dashTimer <= 0.1f && m_Input.DashPressed == true)
            {
                move *= dashSpeed;
                dashTimer = dashCooldown;
                //Debug.LogWarning("dash");
            }
            m_Input.DashPressed = false;

            //move player
            m_Rigidbody.velocity = new Vector3(move.x * movementSpeed, m_Rigidbody.velocity.y, move.z * movementSpeed);
            //m_Rigidbody.AddForce(move * movementSpeed);

            //influence slowdown mechanic
            if (move.magnitude != 0f || !isGrounded)
                m_Time.setSlowdownTime(false);
            else
                m_Time.setSlowdownTime(true);
        }

        private void CheckDashCooldown()
        {
            if (dashTimer > 0.0f)
            {
                dashTimer -= Time.deltaTime;
            }
            else
            {
                dashTimer = 0.0f;
            }
        }

        void CheckGroundStatus()
        {
            if (isGrounded)
                return;

            RaycastHit hitInfo;
#if UNITY_EDITOR
            // helper to visualise the ground check ray in the scene view
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
            // 0.1f is a small offset to start the ray from inside the character
            // it is also good to note that the transform position in the sample assets is at the base of the character
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
            {
               isGrounded = true;
               //m_Animator.applyRootMotion = true;
            }
            else
            {
               isGrounded = false;
               //m_Animator.applyRootMotion = false;
            }

            Debug.LogWarning("grounded? " + isGrounded);
        }

    }
}
