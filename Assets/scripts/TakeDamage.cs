﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyCharacter.EnemyAI))]
public class TakeDamage : MonoBehaviour {

    public Material normalMat, damageMat;

    private float maxHealth = 100;
    private float currentHealth;

    public float defenseValue;

    private EnemyCharacter.EnemyAI m_StateMachine;

    // Use this for initialization
    void Start () {
        currentHealth = maxHealth;
        m_StateMachine = GetComponent<EnemyCharacter.EnemyAI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Damage(float attackValue, bool killAttack)
    {
        Debug.LogWarning("enemy damage function");

        //can't kill the enemy 
        if (!killAttack || (killAttack && (currentHealth > (maxHealth * ApplicationModel.enemyKOPercentage))))
        {
            //light attack
            if (attackValue > ApplicationModel.lightAttackValue)
                StartCoroutine(DisplayDamage(1f));
            //heavy attack
            else
                StartCoroutine(DisplayDamage(0.5f));
        }
        else
        {
            //kill enemy here
            StartCoroutine(DisplayDamage(2f));
        }

        //put this inside the if statement for taking damage
        //currentHealth -= (attackValue - defenseValue);
        //CheckHealth();
    }

    private void CheckHealth()
    {
        if (currentHealth < (maxHealth * ApplicationModel.enemyKOPercentage))
            m_StateMachine.AiState = EnemyCharacter.ENEMYSTATES.KO;


    }

    private IEnumerator DisplayDamage(float seconds)
    {
        Debug.LogWarning("displaydamage function");

        GetComponent<Renderer>().material = damageMat;

        yield return new WaitForSeconds(seconds);

        GetComponent<Renderer>().material = normalMat;

/*disable this when sprint done*/        if (seconds >= 1.5f)
/*disable this when sprint done*/            Destroy(gameObject);
    }
}
