﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemyCharacter
{
    public class OilEnemyAI : EnemyAI
    {
        public bool isLeader;
        public GameObject leader;
        private float rangeAroundLeader = 20.0f;

        //public bool IsLeader
        //{
        //    get
        //    {
        //        return isLeader;
        //    }

        //    set
        //    {
        //        isLeader = value;
        //    }
        //}

        //public GameObject Leader
        //{
        //    get
        //    {
        //        return leader;
        //    }

        //    set
        //    {
        //        leader = value;
        //    }
        //}

        // Use this for initialization
        void Start()
        {
            //setup the sphere collider as a trigger with the detailed perception radius
            perceptionCollider = GetComponent<SphereCollider>();
            perceptionCollider.isTrigger = true;
            //perceptionCollider.radius = perceptionRadius;

            //get the navagent and positions for the wandering behaviour
            navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            startPosition = transform.position;
            targetPosition = startPosition;

            rangedAttackDelayTimeMax = 1.0f + Random.value * 5.0f;
            rangedAttackDelayTime = rangedAttackDelayTimeMax;

            //m_Target = GameObject.FindGameObjectWithTag("Player");
            regainTimeMax = (Random.value * 2.5f) + 4.0f;

            if (isLeader)
                regainTimeMax *= 2;

            regainTime = regainTimeMax;

            startPosition = transform.position;

            //aiState = ENEMYSTATES.IDLE;
        }

        // Update is called once per frame
        void Update()
        {
            //faint animation will be done here?
            //use animation.isPlaying, if it exists

            //Debug.Log(aiState);

            switch (aiState)
            {
                case ENEMYSTATES.IDLE:
                    Wander();
                    break;
                //case ENEMYSTATES.MERGE:
                //    break;
                case ENEMYSTATES.COMBAT:
                    DoAttack();
                    break;
                case ENEMYSTATES.KO:
                    DoFaint();
                    break;
                case ENEMYSTATES.DEATH:
                    DoDie();
                    break;
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                //Debug.Log("see player");
                m_Target = other.gameObject;
                aiState = ENEMYSTATES.COMBAT;
            }
        }

        //behaviour does not change if the player goes out of the trigger
        //enemy has to be destroyed or outrun
        private void OnTriggerExit(Collider other)
        {
            
        }

        //wander around the enemies starting position or the leader
        private new void Wander()
        {
            // if enemy is close enough to the target position, change target position
            if ((transform.position-targetPosition).magnitude < positionCloseEnough)
            {
                if (isLeader)
                {
                    float angle = UnityEngine.Random.Range(0, 360);
                    float distanceFromCenter = UnityEngine.Random.Range(0.1f * wanderDistanceMax, wanderDistanceMax);

                    Vector3 position = startPosition + distanceFromCenter * Vector3.right;
                    targetPosition = ApplicationModel.RotateAroundPivot(angle, position, startPosition);
                }
                else
                {
                    float angle = UnityEngine.Random.Range(0, 360);
                    float distanceFromCenter = UnityEngine.Random.Range(0.1f * rangeAroundLeader, rangeAroundLeader);

                    Vector3 position = leader.transform.position + distanceFromCenter * Vector3.right;
                    targetPosition = ApplicationModel.RotateAroundPivot(angle, position, leader.transform.position);
                }

                navAgent.SetDestination(targetPosition);
            }
            
            //move the enemy closer to the target position
            //setDestination here if the AI won't move after one update
        }

        //destroy the enemy if health is depleted
        private new void DoDie()
        {
            //play animation here

            Destroy(gameObject);
        }

        //attack the player if they are in perception range
        private void DoAttack()
        {
            if (m_Target == null)
            {
                AiState = ENEMYSTATES.IDLE;
                return;
            }

            //range attack
            if ((m_Target.transform.position - transform.position).magnitude >= rangeAttackDistance)
            {
                rangedAttackDelayTime -= Time.deltaTime;
                //Debug.Log(rangedAttackDelayTime);

                if (rangedAttackDelayTime <= 0)
                {
                    if (m_Target != null)
                    {
                        Vector3 dir = m_Target.transform.position - transform.position;
                        Debug.LogWarning("rangeAttack no target");
                        RangeAttack(dir);
                    }

                    rangedAttackDelayTime = rangedAttackDelayTimeMax;
                } 
            }
            else
            {
                //close combat
            }
        }

        //get ko'ed if the health gets to low
        private void DoFaint()
        {
            regainTime -= Time.deltaTime;

            if (regainTime <= 0.0f)
            {
                AiState = ENEMYSTATES.IDLE;
            }
        }

    }

}